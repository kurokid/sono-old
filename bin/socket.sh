#!/bin/bash

HOME=/home/apps
HOMELIB=${HOME}/lib
HOMECONF=${HOME}/cfg
HOMELOG=${HOME}/log
HOMEPID=${HOME}/pid

JVM=/usr/java/jre1.8.0_171/bin/java
APPS="core"

if [ "${1}" = "" ]
then
        echo "Usage: socket.sh [start/stop/status]"
else
        case "${1}" in
                start)
                        PID=`ps -ef | grep java | grep CoStudyingSpace.jar | grep socket.cfg | awk {'print $2'}`
                        if [ "$PID" = "" ]
                        then
                                CLASSPATH=${HOMELIB}/jsoup-1.10.1.jar:${HOMELIB}/mysql-connector-java-5.1.7-bin.jar:${HOMELIB}/CoStudyingSpace.jar:${HOMELIB}/log4j-1.2.16.jar:${HOMELIB}/commons-configuration-1.9.jar:${HOMELIB}/commons-lang-2.4.jar:${HOMELIB}/commons-logging-1.1.1.jar:${HOMELIB}/commons-collections-3.1.jar:${HOMELIB}/commons-codec-1.3.jar:${HOMELIB}/commons-httpclient-3.1.jar:${HOMELIB}/commons-dbcp-1.2.1.jar:${HOMELIB}/commons-pool-1.2.jar:${HOMELIB}/httpclient-4.3.6.jar:${HOMELIB}/httpcore-4.3.3.jar:${HOMELIB}/commons-codec-1.10.jar:.
                        
                                ${JVM} -Xmx512M -Xms128M -cp ${CLASSPATH} com.iq.socket.Main ${HOMECONF}/socket.cfg & echo $! > ${HOMEPID}/socket.pid
                                echo "socket started (PID: `cat ${HOMEPID}/socket.pid`)"
                        else
                                echo "socket already running"
                        fi
                        ;;
                stop)
                        PID=`ps -ef | grep java | grep CoStudyingSpace.jar | grep socket.cfg | awk {'print $2'}`
                        if [ "$PID" = "" ]
                        then
                                echo "socket is not running"
                        else
                                kill -9 `cat ${HOMEPID}/socket.pid`
                                #${JVM} -cp ${CLASSPATH} com.gtk.sms.Shutdown ${HOMECONF}/socket.cfg
				echo "socket stoped (PID: `cat ${HOMEPID}/socket.pid`)"
                                rm -f ${HOMEPID}/socket.pid
                        fi
                        ;;
                status)
                        PID=`ps -ef | grep java | grep CoStudyingSpace.jar | grep socket.cfg | awk {'print $2'}`
                        if [ "$PID" = "" ]
                        then
                                echo "socket is not running"
                        else
                                echo "socket is running (PID: ${PID})"
                        fi
                        ;;
                *)
                        echo "Invalid command! Usage: socket.sh [start/stop/status]"
                        ;;
        esac
fi

